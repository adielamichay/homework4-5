import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toMovies(){
    this.router.navigate(['/movies']);
  }
  toHelp(){
    this.router.navigate(['/help']);
  }
  constructor(private router:Router) { }

  ngOnInit() {
  }

}

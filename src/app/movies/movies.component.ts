import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title', 'studio', 'weekendIncome', 'delete'];
  title = "no movie deleted yet"; 
  movies=[];
  studio = "";
  studios = [];

  toDelete(element)//מחיקה בלי פיירבייס
  {
    let start, end = this.movies;
    let id = element.id;
    let i = 0;
    let deleted = [];

    for(let movie of this.movies)
    {
      deleted[i] = movie.id;
      i++;
    }
    
    start = this.movies.slice(0,deleted.indexOf(id));
    end = this.movies.slice(deleted.indexOf(id)+1, this.movies.length+1);
    this.movies = start;
    this.movies = this.movies.concat(end);   
    this.title = element.title;

  }
  filter() {
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            if (this.studio == 'all') {
              this.movies.push(y);
            }
            else if (this.studio == y['studio'].toLowerCase()) {  
              this.movies.push(y);
            }
            else if (this.studio == '') {
              this.movies.push(y);
            }
          }
        )
      }
    )
  }
  toFilter() {
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            if (this.studio == 'all') {
              this.movies.push(y);
            }
            else if (y['studio'] == this.studio) {
              this.movies.push(y);
            }
          }
        )
      }
    )
  }





  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {

    this.db.list('/movies').snapshotChanges().subscribe(
      movies =>
      {
        this.movies = [];
        this.studios = ['all'];
        movies.forEach(
          movie =>
          {
            let m = movie.payload.toJSON();
            m['key'] = movie.key;
            this.movies.push(m);
            let stu = m['studio'];
            if (this.studios.indexOf(stu)== -1) {
              this.studios.push(stu);
            }
          }
        )
      }
    )

  }


}
 

 

 

